import gitlab

def main():
    gl = gitlab.Gitlab("https://gitlab.com/", ssl_verify=False) # no authentication
    project = gl.projects.get("42380418", lazy=True)

    pipeline = project.trigger_pipeline("main", "glptt-0b09b01e523b41880da830f7986398d68760628d", variables={})

    print("[OUT] trigger pipeline ID: #{}".format(pipeline._attrs['id']))
    # print("\n _id_attr: ", pipeline._id_attr)
    # print("\n _attrs: ", pipeline._attrs['id'])
    # print("\n get_id: ", pipeline.get_id())
    

if __name__ == "__main__":
    print("Trigger activated.. ")
    main()


